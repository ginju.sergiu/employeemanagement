package com.example.springrecap.repository;

import com.example.springrecap.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

// Firstly we put the Class that we use, and after type of primary key
public interface EmployeeRepo extends JpaRepository<Employee,Long> {

    Optional<Employee>findEmployeeById(Long id);

}
