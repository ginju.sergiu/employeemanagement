package com.example.springrecap.service;

import com.example.springrecap.exception.UserNotFoundException;
import com.example.springrecap.model.Employee;
import com.example.springrecap.repository.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.UUID;

@Service
public class EmployeeService {
    private final EmployeeRepo employeeRepo;

    @Autowired
    public EmployeeService(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    public Employee addEmployee(Employee employee){
        employee.setEmployeeCode(UUID.randomUUID().toString());
        return employeeRepo.save(employee);
    }

    public List<Employee> findAll(){
        return employeeRepo.findAll();
    }

    public Employee updateEmployee(Employee employee){
        return employeeRepo.save(employee);
    }


    public Employee findEmployeebyId(Long id){
       return employeeRepo.findEmployeeById(id).
               orElseThrow(() -> new UserNotFoundException("User by id "+ id + " was not found"));
    }

    public void deleteEmployee(Long id){
        employeeRepo.delete(employeeRepo.getReferenceById(id));
    }


}
