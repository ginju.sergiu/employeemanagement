package com.example.springrecap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringrecapApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringrecapApplication.class, args);
	}

}
